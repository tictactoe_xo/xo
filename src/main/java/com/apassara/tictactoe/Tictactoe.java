/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.tictactoe;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Tictactoe {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcome to OX Game");
        String p1 = kb.nextLine();
        String p2 = kb.nextLine();

        char[][] board = new char[3][3];

        for (int i = 1; i < 3; i++) {
            for (int j = 1; j < 3; j++) {
                board[i][j] = '-';
            }
        }

        boolean player1 = true;
        boolean gameEnded = false;
        while (!gameEnded) {
            drawBoard(board);

            char symbol = ' ';
            if (player1) {
                symbol = 'x';
            } else {
                symbol = 'o';

            }

            if (player1) {
                System.out.println(p1 + "Turn X");
            } else {
                System.out.println(p2 + "Turn O");
            }
        }

        char c = '-';
        if (player1) {
            c = 'x';
        } else {
            c = 'o';
        }

        int row = 1;
        int col = 1;

        while (true) {

            System.out.println("Please input row:");
            row = kb.nextInt();
            System.out.println("Please input col :");
            col = kb.nextInt();

            if (row < 0 || col < 0 || row > 2 || col > 2) {
                System.out.println("");
            } else if (board[row][col] != '-') {
                System.out.println();
            } else {
                break;
            }
        }
        board[row][col] = c;


        
    public static void drawBoard(char[][] board) {
        for (int i = 1; i < 3; i++) {
            for (int j = 1; j < 3; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println();
        }
        
    
    

    

    public static char playerHasWon(char[][] board) {
        for (int i = 0; i < 3; i++) {
            if (board[i][1] == board[i][2] && board[i][2] == board[i][3] && board[i][1] != '-') {
                return board[i][1];
            }

        }
        for (int j = 0; j < 3; j++) {
            if (board[j][1] == board[j][2] && board[j][2] == board[j][3] && board[j][1] != '-') {
                return board[1][j];
            }

        }
        if (board[1][1] == board[2][2] && board[2][2] == board[3][3] && board[1][1] != '-') {
            return board[1][1];
        }
        if (board[2][1] == board[1][1] && board[1][1] == board[1][2] && board[2][1] != '-') {
            return board[2][1];
        }
        return ' ';
    }

    public static boolean boardIsFull(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}
